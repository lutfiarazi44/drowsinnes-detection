Git global setup

git config --global user.name "Lutfi Arazi"
git config --global user.email "lutfiarazi44@gmail.com"

Create a new repository
git clone https://gitlab.com/lutfiarazi44/drowsinnes-detection.git
cd drowsinnes-detection
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Push an existing folder
cd existing_folder
git init
git remote add origin https://gitlab.com/lutfiarazi44/drowsinnes-detection.git
git add .
git commit -m "Initial commit"
git push -u origin master

Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/lutfiarazi44/drowsinnes-detection.git
git push -u origin --all
git push -u origin --tags
